---
sidebar: false
home: true
heroText: UEditorPlus
actionText: 开始 →
actionLink: /guide.md
features:
- title: Laravel
  details: 全新的UI外观，使用字体图标
- title: 持续迭代
  details: 持续迭代升级，不断完善
- title: 配置增强
  details: 提供更多功能配置支持
footer: Copyright © 2021-2022 ModStart
---

<p align="center">  
  <a href="https://github.com/modstart-lib/ueditor-plus" target="_blank">
    <img alt="License Apache2.0" src="https://img.shields.io/badge/License-Apache2.0-blue">
  </a>
  <a href="https://github.com/modstart-lib/ueditor-plus" target="_blank">
    <img alt="GitHub last release" src="https://img.shields.io/github/v/release/modstart-lib/ueditor-plus">
  </a>
  <a href="https://github.com/modstart-lib/ueditor-plus" target="_blank">
    <img alt="GitHub last commit" src="https://img.shields.io/github/last-commit/modstart-lib/ueditor-plus">
  </a>
</p>

<style type="text/css">
    .home .hero img{
        height:120px;
    }
</style>

<p align="center" style="font-size:30px;font-weight:bold;">
    快速配置，极速使用
</p>

```html
<script id="editor" type="text/plain" style="height:300px;"></script>
<script type="text/javascript" src="/path/to/UEditorPlus/ueditor.config.js"></script>
<script type="text/javascript" src="/path/to/UEditorPlus/ueditor.all.js"></script>
<script>
    var ue = UE.getEditor('editor', {
        // ... 更多配置
    });
</script>
```

<p align="center" style="font-size:30px;font-weight:bold;">
    界面预览
</p>


<p align="center"><img style="max-width:800px;width:100%;" src="https://ms-assets.modstart.com/demo/UEditorPlus_v2.1.0.jpeg" /></p>

